let eachPlanCost = document.querySelectorAll(".plan-cost");
let monthWiseCost;
let stepNumber = 1;
let selectedPlanCostAddons = [];
let sum = 0;

let phoneNumberErrorMessage = document.getElementsByClassName("phone-error");
let phoneNumberInput = document.getElementsByName("phone");
let nameInput = document.getElementsByName("fname");
let nameErrorMessage = document.getElementsByClassName("name-error");
let emailInput = document.getElementsByName("email");
let emailErrorMessage = document.getElementsByClassName("email-error");

let isValidEmail = () => {
  var pattern = /^[\w.-]+@[\w.-]+\.\w+$/;

  return pattern.test(emailInput[0].value);
};

let removeErrorMessage = () => {
  if (phoneNumberInput[0].value.length > 0) {
    phoneNumberErrorMessage[0].innerText = "";
    phoneNumberInput[0].style.borderColor = "";
  }
  if (nameInput[0].value.length > 0) {
    nameErrorMessage[0].innerText = "";
    nameInput[0].style.borderColor = "";
  }
  if (emailInput[0].value.length > 0) {
    emailErrorMessage[0].innerText = "";
    emailInput[0].style.borderColor = "";
  }
};
let userInputValidation = () => {
  if (nameInput[0].value.trim() === "") {
    nameErrorMessage[0].innerText = "This field is required.";
    nameInput[0].style.borderColor = "hsl(354, 84%, 57%)";
  } else {
    removeErrorMessage();
  }

  if (emailInput[0].value.trim() === "" || !isValidEmail()) {
    emailErrorMessage[0].innerText = "This field is required.";
    emailInput[0].style.borderColor = "hsl(354, 84%, 57%)";
  } else {
    removeErrorMessage();
  }

  if (
    phoneNumberInput[0].value.trim() === "" ||
    phoneNumberInput[0].value.length !== 10
  ) {
    phoneNumberErrorMessage[0].innerText = "This field is required.";
    phoneNumberInput[0].style.borderColor = "hsl(354, 84%, 57%)";
  } else {
    removeErrorMessage();
  }
  if (
    (nameInput[0].value && emailInput[0].value && phoneNumberInput[0].value) !==
    ""
  ) {
    stepNumber++;
    changeDiv();
  }
};

let nextStep = (event) => {
  event.preventDefault();
  if (stepNumber === 1) {
    userInputValidation();
  } else {
    stepNumber++;
    changeDiv();
  }
};

let previousStep = (event) => {
  event.preventDefault();
  stepNumber--;
  changeDiv();
};
let changePlan = () => {
  stepNumber = 2;
  changeDiv();
};
let changeDiv = () => {
  let removeActiveClass = document.querySelector(".active-div");
  removeActiveClass.classList.remove("active-div");

  let addActiveClass = document.getElementsByClassName(`${stepNumber}`);
  addActiveClass[0].classList.add("active-div");
  if (stepNumber < 5) {
    let removeActiveStep = document.getElementsByClassName("active-step");
    removeActiveStep[0].classList.remove("active-step");

    let addActiveStep = document.getElementById(`${stepNumber}`);
    addActiveStep.classList.add("active-step");
  }

  if (stepNumber === 3) {
    storingSelectedPlanDetails();
    clearAllAddons();
    addOnCostUpdate();
  }
  if (stepNumber === 4) {
    finishingUpDetails();
  }
};
let clearFlexClass = (a) => {
  a.forEach((b) => {
    b.parentElement.classList.remove("flex-row");
  });
};
let clearAllAddons = () => {
  let addedAddonCost = document.querySelectorAll(".cost");
  let addedAddOns = document.querySelectorAll(".add-on-added");
  addedAddOns.forEach((elem, index) => {
    elem.innerText = "";
    addedAddonCost[index].innerText = "";
  });
  clearFlexClass(addedAddOns);
};
let storingSelectedPlanDetails = () => {
  selectedPlanCostAddons = [];
  let selectedPlan = document.querySelector('input[name="plan"]:checked');
  let nextSibling = selectedPlan.nextElementSibling;
  let selectedPlanName = nextSibling.querySelector("div .plan-name").innerText;
  let selectedPlanCost = nextSibling.querySelector("div .plan-cost").innerText;
  selectedPlanCostAddons.push(selectedPlanName, selectedPlanCost);
};
let addOnCostUpdate = () => {
  let addOnYearlyCost = [10, 20, 20];
  let addOnMonthlyCost = [1, 2, 2];
  let addOnCosts = document.querySelectorAll(".add-on-cost");
  monthWiseCost = eachPlanCost[0].innerText.includes("mo");
  addOnCosts.forEach((addOnCost, index) => {
    if (!monthWiseCost) {
      addOnCost.innerText = "+$" + addOnYearlyCost[index] + "/yr";
    } else {
      addOnCost.innerText = "+$" + addOnMonthlyCost[index] + "/mo";
    }
  });
};
let addBorderToChecked = (e) => {
  let target = e.target;
  if (target !== undefined) {
    if (target.matches('input[name="add-ons"]')) {
      if (target.checked) {
        target.parentElement.classList.add("border-active");
      } else {
        target.parentElement.classList.remove("border-active");
      }
    }
  }
};
let finishingUpDetails = () => {
  let selectedAddons = document.querySelectorAll(
    'input[name="add-ons"]:checked'
  );
  let length = selectedAddons.length;
  if (length !== 0) {
    selectedAddons.forEach((selectedAddon, index) => {
      let nextSibling = selectedAddon.nextElementSibling;
      let selectedAddonName = nextSibling.querySelector("div .add-on-name");
      let selectedAddonCost = nextSibling.querySelector("div .add-on-cost");
      let addonName = document.querySelector(`#add-on-${index + 1}`);
      let addonCost = document.querySelector(`#cost-${index + 1}`);
      addonName.innerText = selectedAddonName.innerText;
      addonCost.innerText = selectedAddonCost.innerText;
      selectedPlanCostAddons.push(selectedAddonCost.innerText);
      addFlexClass(addonName.parentElement);
      addBorderToChecked(selectedAddon.parentElement);
    });
  }

  let planType = document.querySelector(".plan-type");
  planType.innerText = monthWiseCost
    ? selectedPlanCostAddons[0] + "(Monthly)"
    : selectedPlanCostAddons[0] + "(Yearly)";
  let selectedPlanCost = document.querySelector(".selected-plan-cost");
  selectedPlanCost.innerText = selectedPlanCostAddons[1];
  let total = document.querySelector(".sum");

  let totalText = document.querySelector(".total");
  sum = 0;
  selectedPlanCostAddons.forEach((cost, index) => {
    if (index > 0) {
      sum += Number(cost.match(/\d+/g));
    }
  });

  totalText.innerText = monthWiseCost ? "Total(per month)" : "Total(per year)";
  total.innerText = monthWiseCost ? "+$" + sum + "/mo" : "+$" + sum + "/yr";
};
let addFlexClass = (element) => {
  element.classList.add("flex-row");
};
let monthYearActiveClassChange = () => {
  let monthly = document.querySelector(".monthly");
  let yearly = document.querySelector(".yearly");
  yearly.classList.toggle("active-type");
  monthly.classList.toggle("active-type");
};
let displayMonthsfree = (index, text) => {
  let plans = document.querySelectorAll(".plan");
  let element = plans[index].querySelector("div .free-months");
  element.innerText = text;
};
// Changing Plan cost format
let changeCost = () => {
  let yearlyCost = [90, 120, 150];
  let monthlyCost = [9, 12, 15];
  monthWiseCost = eachPlanCost[0].innerText.includes("mo");
  eachPlanCost.forEach((cost, index) => {
    if (monthWiseCost === true) {
      cost.innerText = "$" + yearlyCost[index] + "/yr";
      let text = "2 months free";
      displayMonthsfree(index, text);
    } else {
      cost.innerText = "$" + monthlyCost[index] + "/mo";
      let text = "";
      displayMonthsfree(index, text);
    }
    monthYearActiveClassChange();
  });
};
let nextButtons = document.querySelectorAll(".next-btn");
nextButtons.forEach((button) => {
  button.addEventListener("click", nextStep);
});
let backButtons = document.querySelectorAll(".back-btn");
backButtons.forEach((button) => {
  button.addEventListener("click", previousStep);
});
let toggleButton = document.getElementsByClassName("checkbox");
toggleButton[0].addEventListener("click", changeCost);
let switchPlan = document.querySelector(".change-plan");
switchPlan.addEventListener("click", changePlan);
let addOnContainer = document.getElementsByClassName(
  "add-ons-options-container"
);
addOnContainer[0].addEventListener("click", addBorderToChecked);
